# ahojky - Dneska bude Python

``` py
print("Ahoj")
```

## Spuštění
``` sh
python main.py
```

## Proměnné
``` py
message = "Hello"
print(message)
```

## Funkce
``` py
def say_hello():
    print("ahoj")

say_hello()
```

## Funkce - parametry
``` py
def say_hello(text):
    print(text)

say_hello("hello")
say_hello("hi")

Vypíše:

hello
hi
```
## Funkce - součet
``` py
def soucet(x, y):
    return x + y

z = soucet(10, 20)
print(z)
```
## Faktoriál
``` py
def factorial(n):
    if n == 0 or n == 1:
        return 1
    return n * factorial(n-1)
z = factorial(6)
print (z)
```